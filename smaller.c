#include <stdio.h> 
int main() 
{ 
int A, B, C, smaller; 
printf("Enter three numbers: "); 
scanf("%d %d %d", &A, &B, &C); 
smaller = A < B ? (A < C ? A : C) : (B < C ? B : C); 
printf("%d is the smaller number.", smaller); 
return 0; 
} 