#include <stdio.h>
int main()
{
int n1, n2, n3, k, i;
printf("Enter the number:");
scanf("%d %d %d", &n1,&n2,&n3);
k = n1<n2 ? (n1 < n3 ? n1 : n3) : (n2 < n3 ? n2 : n3);
printf("%d is the smaller\n",k);
for (i=k; i>0; --i)
printf("%d\n",i);
return 0;
}