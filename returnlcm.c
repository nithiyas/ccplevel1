#include<stdio.h>
int find_lcm(int a, int b, int c);
int main()
{
int a,b,c,k,i,n,gcd,lcm;
printf("Enter the value:\n");
scanf("%d %d %d",&a,&b,&c);
lcm = find_lcm(a, b, c);
printf("the lcm of %d , %d, and %d is %d.",a,b,c,lcm);
return 0;
}
int find_lcm(int a, int b, int c)
{
int lcm,i,gcd,n;
for(i=1;i<=a && i<=b; ++i)
{
if(a%i==0 && b%i==0)
n = i;
}
lcm = (a * b)/ n;
for(i=1;i<=lcm && i<=c; ++i)
{
if(lcm%i==0 && c%i==0)
gcd= i;
}
lcm = (lcm*c)/gcd;
return lcm;
}