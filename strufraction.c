#include<stdio.h>
typedef struct fraction
{
    int num;
    int deno;
}frac;
frac input();
int findGCD (int b, int c);
frac simplify(frac a);
frac sum (frac x, frac y);

frac input()
{
    frac f;
    printf("the value of num is\n");
    scanf("%d",&f.num);
    printf("the value of deno is\n");
    scanf("%d",&f.deno);
    return f;
}
frac sum (frac x, frac y)
{
    frac add, s,a;
    if(x.deno == y.deno)
    {
        add.num = x.num + y.num;
        add.deno = x.deno;
    }
    else
    {
        add.num = ((x.num*y.deno) + (y.num*x.deno));
        add.deno= x.deno*y.deno;
    }
    a.num = add.num;
    a.deno = add.deno;
    s = simplify(a);
    return s;
}
int findGCD (int b, int c)
{
    int g = b;
    while (b != c)
    {
        if(b > c)
        {
            b-=c;
            g = b;
        }
        else
        {
            c -= b;
            g = c;
        }
    }
    return g;
}
frac simplify(frac a)
{
    frac s;
    int gcd,b,c;
    b = a.num;
    c = a.deno;
    gcd = findGCD(b,c);
    s.num = b/gcd;
    s.deno = c/gcd;
    return s;
}
void output(frac x, frac y, frac add)
{
    printf("The 1st fraction is %d/%d\n",x.num,x.deno);
    printf("The 2nd  fraction is %d/%d\n",y.num,y.deno);
    printf("The sum of fraction is %d/%d\n",add.num,add.deno);
}
int main()
{
    frac x,y,add;
    x = input();
    y = input();
    add = sum(x,y);
    output(x,y,add);
    return 0;
}