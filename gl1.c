#include<stdio.h>
int find_lcm(int a, int b, int c);
int find_gcd(int a, int b, int c);
int main()
{
int a,b,c,gcd,lcm;
printf("Enter the value:\n");
scanf("%d%d%d",&a,&b,&c);
lcm = find_lcm(a,b,c);
gcd = find_gcd(a,b,c);
printf("The GCD of three numbers %d , %d and %d is %d.\n", a,b,c,gcd);
printf("The LCM of three numbers %d , %d and %d is %d.", a,b,c,lcm);
return 0;
}
int find_lcm(int a, int b, int c)
{
int i,n,k,gcd,lcm;
for(i=1;i<=a && i<=b; ++i)
{
if(a%i==0 && b%i==0)
n = i;
}
k = (a * b)/n;
for(i=1;i<=k && i<=c; ++i)
{
if(k%i==0 && c%i==0)
gcd = i;
}
lcm = (k*c)/gcd;
return lcm;
}
int find_gcd(int a, int b, int c)
{
int i,gcd;
for(i=1;i<=a && i<=b && i<=c; ++i)
{
if(a%i==0 && b%i==0 && c%i==0)
gcd = i;
}
return gcd;
}