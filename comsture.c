#include <stdio.h>
#include <math.h>
struct complex
{
    float real;
    float img;
};
typedef struct complex Complex;

Complex input() 
{
    Complex c;
    printf("Enter real\n");
    scanf("%f",&c.real);
    printf("Enter img\n");
    scanf("%f",&c.img);
    return c;
}

Complex compute(Complex cnum1, Complex cnum2)
{
    Complex sum;
    sum.real = cnum1.real + cnum2.real;
    sum.img = cnum1.img + cnum2.img;
    return sum;
}

void output (Complex c3)
{
    printf("%.2f + %.2fi is complex",c3.real,c3.img);
}

int main(void)
{
    Complex c1,c2,c3;
    c1=input();
    c2=input();
    c3=compute(c1,c2);
    output(c3);
    return 0;
}
